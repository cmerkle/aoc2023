'''with open("day4.txt") as file:
    data = file.read().split('\n')

lines = [i.split('|') for i in data]

total = []

points = []
found = False
total = 0

for number in lines:
    running_total = 0
    counter = 0
    winning_numbers = [int(i) for i in number[0].split() if i.isdigit()]
    my_numbers = [int(i) for i in number[1].split() if i.isdigit()]

    for win_num in winning_numbers:

        if win_num in my_numbers:
            counter += 1
            
        if counter == 1:
            running_total = 1


        elif counter == 2:
            running_total = 2

        elif counter == 3:
            running_total = 4

        elif counter == 4:
            running_total = 8

        elif counter == 5:
            running_total = 16

        elif counter == 6:
            running_total = 32

        elif counter == 7:
            running_total = 64

        elif counter == 8:
            running_total = 128

        elif counter == 9:
            running_total = 256

        elif counter == 10:
            running_total = 512

        elif counter > 10:
            print("What have you done?")

        #else:
         #   print("No winning numbers.")

    total += running_total

    #print(running_total)
    print(total)
'''
#Pt 2
with open("day4sample.txt") as file:
    data = file.read().split('\n')

lines = [i.split('|') for i in data]

total = []

#while True:
#counter = 0
points = []
found = False
total = 0

for number in lines:
    running_total = 0
    counter = 0
    winning_numbers = [int(i) for i in number[0].split() if i.isdigit()]
    my_numbers = [int(i) for i in number[1].split() if i.isdigit()]
    card_index = 0

    for win_num in winning_numbers:

        if win_num in my_numbers:
            counter += 1
            
        if counter == 1:
            running_total = 1
            add_cards = 

        elif counter == 2:
            running_total = 2

        elif counter == 3:
            running_total = 4

        elif counter == 4:
            running_total = 8

        elif counter == 5:
            running_total = 16

        elif counter == 6:
            running_total = 32

        elif counter == 7:
            running_total = 64

        elif counter == 8:
            running_total = 128

        elif counter == 9:
            running_total = 256

        elif counter == 10:
            running_total = 512

        elif counter > 10:
            print("What have you done?")

        #else:
         #   print("No winning numbers.")
    print(counter)

    #total += running_total

    #print(running_total)
    #print(total)

#combined part 1 and part 2. part 2 adds in new_data list

with open("day1pt1.txt") as file:
        data = [i for i in file.read().strip().split("\n")]

new_data = []

#block shared letters from being positioned together by inserting number value between. first will still be counted this way
#unsure if faster way to replace without listing each one? research
for i in data:
      i = i.replace("one", "1one")
      i = i.replace("two", "two2")
      i = i.replace("three", "thre3e")
      i = i.replace("four", "fou4r")
      i = i.replace("five", "5five")
      i = i.replace("six", "si6x")
      i = i.replace("seven", "seve7n")
      i = i.replace("eight", "8eight")
      i = i.replace("nine", "nine9")
      i = i.replace("zero", "zer0o")
      new_data.append(i)


num_pairs = []

for number in new_data:
    number1 = next(int(i) for i in number if i.isdigit())
    number2 = next(int(i) for i in reversed(number) if i.isdigit())

    num_pairs.append(str(number1) + str(number2))

    int_list = []
    for i in num_pairs:
          i = int(i)
          int_list.append(i)
    

    total = sum(int_list)

print(total)
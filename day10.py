with open ('day10sample.txt') as file: #opens map data as file
    map = file.read().split() #split data at newlines

#library listing all possible movements based on pipe style
movements = {'|' : [(1, 0), (-1, 0)], '-' : [(0, 1), (0, -1)], 'L' : [(-1, 0), (0, 1)], 'J' : [(-1, 0), (0, -1)], '7' : [(1, 0), (0, -1)], 'F' : [(1, 0), (0, 1)]}

#s start is 120,110
'''x_coor = 120
y_coor = 110
s_start = map[x_coor][y_coor]'''

for i, j in enumerate(map):
    if 'S' in j:
        y_s = j.index('S')
        x_s = i
        print(x_s, y_s)

#s_down
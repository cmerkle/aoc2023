def is_valid_move(grid, x, y, visited):
    rows, cols = len(grid), len(grid[0])
    return 0 <= x < rows and 0 <= y < cols and grid[x][y] != '#' and not visited[x][y]

def dfs(grid, x, y, visited, length):
    directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]  # Right, Down, Left, Up

    max_length = length
    for dx, dy in directions:
        new_x, new_y = x + dx, y + dy

        if is_valid_move(grid, new_x, new_y, visited):
            visited[new_x][new_y] = True
            new_length = dfs(grid, new_x, new_y, visited, length + 1)
            max_length = max(max_length, new_length)
            visited[new_x][new_y] = False

    return max_length

def find_longest_hike(grid):
    rows, cols = len(grid), len(grid[0])
    visited = [[False] * cols for _ in range(rows)]

    for i in range(cols):
        if grid[0][i] == 'S':
            start_x, start_y = 0, i

    visited[start_x][start_y] = True
    return dfs(grid, start_x, start_y, visited, 1)

# Puzzle input
puzzle_input = """
#S#####################
#OOOOOOO#########...###
#######O#########.#.###
###OOOOO#OOO>.###.#.###
###O#####O#O#.###.#.###
###OOOOO#O#O#.....#...#
###v###O#O#O#########.#
###...#O#O#OOOOOOO#...#
#####.#O#O#######O#.###
#.....#O#O#OOOOOOO#...#
#.#####O#O#O#########v#
#.#...#OOO#OOO###OOOOO#
#.#.#v#######O###O###O#
#...#.>.#...>OOO#O###O#
#####v#.#.###v#O#O###O#
#.....#...#...#O#O#OOO#
#.#########.###O#O#O###
#...###...#...#OOO#O###
###.###.#.###v#####O###
#...#...#.#.>.>.#.>O###
#.###.###.#.###.#.#O###
#.....###...###...#OOO#
#####################O#
"""

# Clean up the input and convert it to a list of lists
grid = [list(line.strip()) for line in puzzle_input.strip().split('\n')]

# Find the longest hike
longest_hike_length = find_longest_hike(grid)
print("The number of steps in the longest hike is:", longest_hike_length)

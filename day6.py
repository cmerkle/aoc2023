time = [50, 74, 86, 85] #times for each race
distance = [242, 1017, 1691, 1252] #current winning distances
totals = [] #new list to add possible winning numbers

for i in range(len(time)):
    hold_time = 0 #holding time for button must start at 1
    ways_to_win = 0
    while hold_time < time[i]: #must be less than total time
        hold_time += 1
        score = (time[i] - hold_time) * hold_time #time holding button takes away from race time but multiplies speed
        if score > distance[i]:
            ways_to_win += 1
    totals.append(ways_to_win)

result = 1
for i in totals:
    result = result * i
print(result)


'''increases 1 ms held
hold > 0 and hold < racetime

holdtime = must be > 0 and < time
time = time defined in table
racetime = (totaltime - holdtime)(holdtime)

to be winner, racetime must exceed distance (chart)'''
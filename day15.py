with open ("day15.txt") as file:
    data = file.read().split(',')

num_list = []
start_num = 0
modulo = 256
step_two = 17

for i in data:
    cur_value = 0
    for j in i:
        cur_value += (ord(j)) 
        cur_value = cur_value * step_two
        cur_value = cur_value%modulo
    num_list.append(cur_value)

print(sum(num_list))

#goal: assign point values to each hand in a game. 
#hands with same value then get ranked based on highest card
#once ordered, cards get rank from 1-x (total games),1 as last place, multiplying their bet by their rank.
#find total winnings by adding each hands winnings together
with open("day7.txt") as file:
    data = file.read().split('\n')

three_kind_list = []
four_kind_list = []
five_kind_list = []

lines = [i.split(' ') for i in data]
index = 0
for i in lines:
    temp_list = []

    bet = i[1]
    hand = i[0]
    #print(hand)

    #-------------Five of a Kind---------------
    if hand.count(hand[index]) == 5:
        temp_list.append(bet)
        temp_list.append(hand)
        five_kind_list.append(temp_list)   

    #-------------Four of a Kind----------------
    elif hand.count(hand[index]) == 4:
        temp_list.append(bet)
        temp_list.append(hand)

        four_kind_list.append(temp_list)

    #-------------Three of a Kind---------------
    elif hand.count(hand[index]) == 3:
        temp_list.append(bet)
        temp_list.append(hand)
        three_kind_list.append(temp_list)
    
    index += 1
    #print(five_kind_list)
    #print(four_kind_list)
    print(three_kind_list)

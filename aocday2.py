#----------------------AoC Day 2----------------------
with open("day2pt1.txt") as file:
    data = [i for i in file.read().strip().split("\n")]
    #print(data)

#possible cubes for any 1 draw within the 3 games of each set
red_num = 12
green_num = 13
blue_num = 14

#-------------------pt 1--------------------
game_counter = []
game_total = 0
valid = True
final = 0

'''for i in data:
    split_data = i.split(':')
    values = split_data[1].split()
    game_num = split_data[0].replace('Game ', '')
    #print(values)
    valid = True
    for i in range(len(values)):
        if 'blue' in values[i] and int(values[i-1]) > blue_num:
            valid = False
        elif 'green' in values[i] and int(values[i-1]) > green_num:
            valid = False
        elif 'red' in values[i] and int(values[i-1]) > red_num:
            valid = False
    if valid == True:
        game_counter.append(game_num)

for i in game_counter:
    game_total += int(i)

print(game_total)'''


for i in data:
    split_data = i.split(':')
    values = split_data[1].split()
    #print(values)
    valid = True
    blue_values = []
    green_values = []
    red_values = []
    for i in range(len(values)):
        blue_cube = 0
        red_cube = 0
        green_cube = 0
        if 'blue' in values[i]:
            blue_cube = int(values[i-1])
            blue_values.append(blue_cube)
        elif 'green' in values[i]:
            green_cube = int(values[i-1])
            green_values.append(green_cube)
        elif 'red' in values[i]:
            red_cube = int(values[i-1])
            red_values.append(red_cube)
    blue_cube = max(blue_values)
    red_cube = max(red_values)
    green_cube = max(green_values)
    
    cube_value = red_cube * blue_cube * green_cube
    final += cube_value
print(final)
    

#----------------pt 2-------------------
'''for game_num in data:
    split_data = game_num.split(';')
    game_1 = split_data[0].split()
    game_2 = split_data[1].split()
    game_3 = split_data[-1].split()

    blue_values = []
    red_values = []
    green_values = []

    for i in range(len(game_1)):
        bg1 = 0
        gg1 = 0
        rg1 = 0
        if 'blue' in game_1[i]:
            bg1 = int(game_1[i-1])
            blue_values.append(bg1)
        if ' green' in game_1[i]:
            gg1 = int(game_1[i-1])
            green_values.append(gg1)
        if 'red' in game_1[i]:
            rg1 = int(game_1[i-1])
            red_values.append(rg1)

    for i in range(len(game_2)):
        bg2 = 0
        gg2 = 0
        rg2 = 0
        if 'blue' in game_2[i]:
            bg2 = int(game_2[i-1])
            blue_values.append(bg2)
        if 'green' in game_2[i]:
            gg2 = int(game_2[i-1])
            green_values.append(gg2)
        if 'red' in game_2[i]:
            rg2 = int(game_2[i-1])
            red_values.append(rg2)

    for i in range(len(game_3)):
        bg3 = 0
        gg3 = 0
        rg3 = 0
        if 'blue' in game_3[i]:
            bg3 = int(game_3[i-1])
            blue_values.append(bg3)
        if 'green' in game_3[i]:
            gg3 = int(game_3[i-1])
            green_values.append(gg3)
        if 'red' in game_3[i]:
            rg3 = int(game_3[i-1])
            red_values.append(rg3)

    
    blue_cube = max(blue_values)
    red_cube = max(red_values)
    green_cube = max(green_values)
    
    cube_value = red_cube * blue_cube * green_cube
    #print(blue_cube)
    print(cube_value)'''